#need a cleaner overall approach
#it's basically segmentation on vectors
#so i can clean it up to allow many experimetns of diff techniques to be run easily
#and more organised
#
import math

class Master(dict):
	def average(self):
		self.avg = 0.0
		self.total = 0.0
		lself = len(self)
		if lself != 0:
			for k in self:
				self.avg += self[k]
				self.total += 1.0/self[k]
			self.avg /= lself

import re

class Parse:
	def __init__(self,master,text,deflate):
		self.master = master
		self.raw_text = text
		self.non_breaking_abbreviations = { #abbrevs that usually involve a stop but don't end the sentence
				'mr':1,'dr':1,'ms':1,'mrs':1,'sr':1,'jr':1,'hon':1,'mp':1,'lt':1,'co':1,'gen':1,'gov':1,'no':1,'nom':1,'vol':1,
				'addr':1,'tel':1,'fl':1,'il':1,'ma':1,'mme':1,'mssrs':1,'mlle':1,'mil':1,'km':1,'kilo':1,'oz':1,'lt':1,'ca':1,'ny':1,
				'mi':1,'fr':1,'sen':1,'esq':1,'adj':1,'alt':1

		}
		self.words = self.words_from_raw_text()
		self.swords = self.aggregate_word_scores()
		self.sentences = self.sentences_from_words()
		self.segments = self.vectors_from_sentences()
		self.top_vectors = self.normalize_vectors()
		self.top_segments = self.score_segments()
		self.o_segs = self.segs_from_normal_vectors()
		#self.skim_history = self.skim(deflate,3)

	def normalize_vectors(self):
		maxdelta = self.vectors[0][1]
		mindelta = maxdelta
		for v in self.vectors:
			vdelta = v[1]
			if vdelta < mindelta:
				mindelta = vdelta
			if vdelta > maxdelta:
				maxdelta = vdelta
		deltarange = 1.0*(maxdelta - mindelta)
		for v in self.vectors:
			v[1] -= mindelta
			v[1] /= deltarange
			if v[1] != 0:
				v[1] = math.log(v[1])
		return self.vectors

	def segs_from_normal_vectors(self):
		segs = []
		seg = []
		avg = -5
		lvalue = 1.0 
		for nv in self.top_vectors:
			value = nv[1]
			ratio = lvalue/(1.0+nv[1])
			#print ratio,nv[2]
			#i = sys.stdin.readline()
			if ratio > 0.5 and ratio < 2.0:
				seg.append(ratio)
				seg.append(nv[2])
			elif ratio > 2.0:
				seg.append(ratio)
				seg.append(nv[2])
				segs.append(seg)
				print seg
				seg = []
			else:
				print seg
				segs.append(seg)
				seg = [ratio,nv[2]]
			lvalue = value
		segs.append(seg)
		return segs

	def processed_word(self,raw_word,raw_index):
		p_word = raw_word.lower()
		try:
			p_word.encode('ascii')
		except:
			return None
		if len(p_word.strip()) == 0 or re.match(r'\W+', p_word):
			return None
		return p_word
		


	def info_list_from_word(self,word,index_score,index_score_sum):
		if word in self.freq:
			self.freq[word] += len(word)*(index_score*index_score)/index_score_sum
		else:
			self.freq[word] = len(word)*(index_score*index_score)/index_score_sum
		master_score = self.master.avg/self.master.total
		ms = master_score
		if word in self.master:
			ms = self.master[word]
			master_score = (1.0/self.master[word])/self.master.total
		master_score = -math.log(master_score)*master_score
		return [word, master_score,ms]

	def aggregate_word_scores(self):
		wd = {}
		for word in self.words:
			wordt = word[0]
			words = word[1]
			if re.match(r'[\s\W]+',wordt):
				continue
			wordf = self.freq[wordt]
			wordf /= (1.0*len(self.words))
			wordf = -math.log(wordf)*wordf	
			scale = 1.0
			if re.match(r'[\d,\.-]+',wordt):
				scale = 1.0/256
			#print wordf,' ',words
			if wordt not in wd:
				scope = -len(self.words)*1.0/1500000
				scope = wordf**(5.5 + scope)
				wd[wordt] = [wordf,words,scale*(math.sqrt(abs(scope - words*words))/math.sqrt(wordf*words))]
		self.ss = wd
		return sorted(wd.iteritems(),reverse=True,key=lambda(x):x[1][2])
	
	def words_from_raw_text(self):
		words = []
		self.freq = {}
		self.raw_text = self.raw_text.replace('...', ' - ')
		self.raw_text = self.raw_text.replace('--', ' - ')
		raw_words = re.findall(r"\w+[\w\.,-]+\w|\w\w|\w|[^\w\s]|\s+", self.raw_text,re.UNICODE)
		lrw = len(raw_words)
		index_score = 1.0
		index_score_sum = 1.0
		index_score_delta = 1.0/lrw
		for raw_index in xrange(lrw):
			raw_word = raw_words[raw_index]
			word = self.processed_word(raw_word,raw_index)
			if word is not None:
				info_list = self.info_list_from_word(word,index_score,index_score_sum)
				index_score -= index_score_delta
				index_score_sum += index_score
				words.append(info_list)
			else:
				continue
		#for info_list in words:
		#	info_list[3] = 1.0/self.freq[info_list[0]]
		return words

	def ends_a_sentence(self,info_tuple,word_index):
		word = info_tuple[0]
		#print info_tuple 
		if word[-1] == '.':
			#caveats for stops
			if len(word) > 1 and word.find('.') < len(word)-1: #i.e. there is another stop in the word like U.S.
				#print "Another stop in this word"
				return False
			elif word_index > 0: #check the previous word if it exists
				lword = self.words[word_index-1][0]
				if len(lword) == 1 or '.' in lword:	#i.e. so either an initial like J. Ito or 
								   	#either stop itself or contains stop like B.I.G in this case probably not a sentence end
					#print "Initial"
					return False
				if lword in self.non_breaking_abbreviations:
					#print "NBabbrev"
					return False
			elif word_index < len(self.words)-1: #check the next word if it exists
				nword = self.words[word_index+1][0]
				if nword == ',' or nword == '.' : #so ., or an ellipsis tridot :)
					#print "Continuer"
					return False
			#passed all the caveats so
			#print "Ender"
			return True
		else:
			#print "Not even a stop"
			return False

	def sentences_from_words(self):
		sentences = []
		sentence = []
		lw = len(self.words)
		for word_index in xrange(lw):
			word = self.words[word_index]
			if re.match(r'\s+',word[0]):
				continue
			if self.ends_a_sentence(word,word_index):
				sentence.append(word)
				sentences.append(sentence)
				sentence = []
			else:
				sentence.append(word)
				continue
		sentences.append(sentence)
		return sentences

	def ends_a_segment(self,word_info_list,last_w):
		#we perform a calculation based on the ratio
		#of metrics in this word and the previous word
		#first we check if this is the first word
		if last_w == None:
			return False
		#if not we check for repetition
		if word_info_list[0] == last_w[0] and len(last_w[0]) > 1: 	#repeats or doubles are assumed to be headings, anyway they are not allowed as continuers
										#so we break a segment on them
										#so ... will not break but
										#jay-z jay-z will break
			return True
		#then if we get here we calculate each words value at this point
		#	
		val_word = word_info_list[-1]#word_info_list[1]*word_info_list[2]*word_info_list[3]
		val_l_word = last_w[-1]#last_w[1]*last_w[2]*last_w[3]
		#then we check the ratio
		val_ratio = val_word/val_l_word 
		#then we check if this ratio is above a threshold, we can adapt this threshold to get segments of a particular characteristic
		#say of a particular length
		#print word_info_list[0],round(val_ratio,2)
		return val_ratio

	def text_from_segment(self,segment):
		words = []
		for word_info_list in segment:
			words.append(word_info_list[0])
		return ' '.join(words)

	def vectors_from_sentences(self):
		#based on word / last word comparisons we determine if a segment occurs or not
		vector = []
		segments = {}
		segment = []
		lword = ['start',1.0]
		for sentence in self.sentences:
			for word_info_list in sentence:
				result = self.ends_a_segment(word_info_list,lword)
				vector.append([lword[0],result,word_info_list[0]])
				#print result
				#maybe another feature determines which way we place these
				#but this placement certainly seems to find 'relevance'
				if result > 3300000000 or result < 1.0/3300000000 :
					hardseg = ''
					if result < 1 :
						hardseg = self.text_from_segment(segment)
						#print hardseg
						if hardseg in segments:
							segments[hardseg][1] += 1 # we could use scale, like the scale of the word, wprd_info_list[1]
						else:
							segments[hardseg] = [segment,1]
						segment = [word_info_list]
					elif result >= 1:
						segment.append(word_info_list)
						hardseg = self.text_from_segment(segment)
						#print hardseg
						if hardseg in segments:
							segments[hardseg][1] += 1 # we could use scale, like the scale of the word, wprd_info_list[1]
						else:
							segments[hardseg] = [segment,1]
						segment = []
					#i = sys.stdin.readline()
				else:
					segment.append(word_info_list)
				lword = word_info_list
		hardseg = self.text_from_segment(segment)
		if hardseg in segments:
			segments[hardseg][1] += 1 # we could use scale, like the scale of the word, wprd_info_list[1]
		else:
			segments[hardseg] = [segment,1]
		self.vectors = vector
		return segments
	
	def score_segments(self):
		sss = []
		for hseg in self.segments:
			spesh = self.segments[hseg]
			seg_score = 0.0
			for wordis in spesh[0]:
				if wordis[0] not in self.ss:
					continue
				scoreholder = self.ss[wordis[0]]
				seg_score += scoreholder[-1]
			sss.append([hseg,seg_score])
		sss = sorted(sss,reverse=True,key=lambda(x):x[1])
		return sss

	#########################################
	##
	##
	##
	##
	##
	##
	##
	##
	##
	##
	##below this line is doucment summarization

	def find_biggest_changes(self,wlist,num):#errorneous it is findingt he biggest vals not the biggest changes
		if len(wlist) == 0:
			return [0.0]
		maxvals = [wlist[0][4]]
		for w in wlist:
			wval = w[4]
			if wval < maxvals[0]:
				maxvals.insert(0,wval)
			elif wval > maxvals[-1]:
				maxvals.insert(len(maxvals),wval)
			else:
				maxvals.append(wval)
				maxvals = sorted(maxvals)
		return maxvals[len(maxvals)-num:]

	def list_segment_on(self,wlist,cutoff):
		segments = []
		segment = []
		for w in wlist:
			wval = w[4]
			if wval >= cutoff:
				if len(segment) > 0:
					print self.text_from_segment(segment)
					segments.append(segment)
					segment = []
			else:
				ws = w[0].strip()
				if len(ws) > 0:
					segment.append(w)
		if len(segment) > 0:
			print self.text_from_segment(segment)
			segments.append(segment)
		return segments

	def seg_segment_on(self,seglist,cutoff):
		segments = []
		segment = []
		for seg in seglist:
			for w in seg:
				wval = w[4]
				if wval >= cutoff:
					if len(segment) > 0:
						print self.text_from_segment(segment)
						segments.append(segment)
						segment = []
				else:
					ws = w[0].strip()
					if len(ws) > 0:
						segment.append(w)
			if len(segment) > 0:
				print self.text_from_segment(segment)
				segments.append(segment)
				segment = []
		return segments

	def remove_words_above(self,wlist,cutoff):
		new_wlist = []
		removed = []
		for w in wlist:
			wval = w[4]
			if wval >= cutoff:
				removed.append(w)
			else:
				new_wlist.append(w)
		return new_wlist,removed

	def collect_words_from_segs(self,seglist):
		words = []
		for seg in seglist:
			for w in seg:
				words.append(w)

		return words

	def skim(self,minpc,change_rank):
		skim_history = []
		local_words = []
		for word_info in self.words:
			local_words.append(word_info)
		minpc = round(minpc/100.0*len(local_words))
		literative_segments = [local_words]
		llit_segments = local_words
		seg_words = local_words
		while len(seg_words) > minpc:
			#find the smallest of the change_rank biggest changes
			smallest_biggest_change = min(self.find_biggest_changes(local_words,change_rank))
			#do an iteration on a list that gets mashed together each time borders are permeable
			lit_segments = [] #self.list_segment_on(llit_segments,smallest_biggest_change)
			llit_segments = self.collect_words_from_segs(lit_segments)
			#do an iteration where borders are irreversible, once cut, no longer can be introduced
			iterative_segments = [] #self.seg_segment_on(literative_segments,smallest_biggest_change)
			literative_segments = iterative_segments
			#do an non iterative only cutting back the list each time based on the new cutoff
		 	initial_segments = self.list_segment_on(self.words,smallest_biggest_change)
			seg_words = self.collect_words_from_segs(initial_segments)
			#iteration step for iterative segments
			local_words,breaking_words = self.remove_words_above(local_words,smallest_biggest_change)
			#commit these changes to our history
			skim_history.append([lit_segments,iterative_segments,initial_segments,breaking_words]) #i am interested to see if iterative == initial thru the loop	
		return skim_history

import sys
import json

def main():
	print "Opening master dict...",
	mdictfile = open('real','r')
	print "Done"
	print "Loading master dict...",
	master = Master(json.load(mdictfile))
	print "Done "+ `len(master)`
	print "Averaging master..."	
	master.average()
	print "Done " + `master.avg`
	print "Opening file (" + sys.argv[1] +")...",
	itext = open(sys.argv[1],'r').read()
	print "Done " + `len(itext)` 
	print "Processing file..."
	if len(sys.argv) < 3:
		p = Parse(master,itext,None)
	else:
		p = Parse(master,itext,int(sys.argv[2]))
	print "Done"
	print "Displaying processed, scored, aggregated and sorted words..."
	for word in p.swords:
		print word
		i = sys.stdin.readline()
	#print "Done " + `len(p.words)`
	#print "Displaying sentences..."
	#for sentence in p.sentences:
	#	for word in sentence:
	#		print word[0],
	#	print
	#	i = sys.stdin.readline()
	#print "Done "+`len(p.sentences)`
	#print "Displaying segments..."
	#for hardsegment in p.segments:
	#	print hardsegment
	#	i = sys.stdin.readline()
	#print "Done "+`len(p.segments)`
	#print "Displaying top segments..."
	for top in p.top_segments:
		print top
		i = sys.stdin.readline()
	#print "Done "+`len(p.top_segments)`
	#fo = open('tseries','w')
	#for v in p.top_vectors:
	#	print >>fo, v[0],v[2],'^',v[1]
	#	
	#fo.close()
	for os in p.o_segs:
		print os
		i = sys.stdin.readline()

	"""
	print "Displaying skim history . . ."
	i = 0
	for history_slice in reversed(p.skim_history):
		i += 1
		litr_segs,itr_segs,ini_segs,rem_word = history_slice
		print "Skim history " + `i` + ": displaying list iterative segs..."
		for seg in litr_segs:
			print p.text_from_segment(seg),
			#a = sys.stdin.readline()
		print "Done "+`len(litr_segs)`
		print "Skim history " + `i` + ": displaying seg iterative segs..."
		for seg in itr_segs:
			print p.text_from_segment(seg),
			#a = sys.stdin.readline()
		print "Done "+`len(itr_segs)`
		print "Skim history " + `i` + ": displaying initial segs..."
		for seg in ini_segs:
			print p.text_from_segment(seg),
			#a = sys.stdin.readline()
		print "Done "+`len(ini_segs)`
		print "Skim history " + `i` + ": " + `len(rem_word)` + " removed words ",
		print rem_word
		print "Strike enter to continue. Eat fresh!"
		a = sys.stdin.readline()
	print "Done "+`len(p.skim_history)`
	"""
	print "Exiting..."

if __name__ == "__main__":
	main()

