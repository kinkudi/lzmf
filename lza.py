import codecs
import sys
import operator

#ith simple ascending code and online progress report
wordmode = True
gcount = 0
code = 0
def next_code():
	global code
	code += 1
	return code

table = {}
word = ''
lengthcount = 0
runningcount = 0

def clean_dict():
	#delete all the hapax legomonon
	tabs = sorted(table.iteritems(), key=lambda(k,v):v[1])
	hapaxlegomenon = {}
        numberentry = 0	
	for entry in tabs:
		numberentry += 1
		if numberentry > 1000000 :
			hapaxlegomenon[entry[0]] = 1
	for entry in hapaxlegomenon.iteritems():
		del table[entry[0]]


def next_code():
	global code
	code += 1
	return code

def show_status(fof):
	global code,lengthcount,runningcount
	#print ("\nAverage code length: %.2f" % (lengthcount*1.0/runningcount))
	#print "\nCode up to:" +`code`
	#a = raw_input("\nEnter continues, anything else then enter displays all codes\n")
	if True: #len(a) >= 0:
		count = 0
	        sorted_table = sorted(table.iteritems(), key=lambda(k,v):v[1])
		for entry in reversed(sorted_table):
			aword = entry[0]
			codefreq = entry[1]
			try:
				fof.write(', '.join([aword ,`codefreq[0]`,`codefreq[1]`,"\n"]))
			except UnicodeEncodeError:
				sys.stderr.write("STDERR:Got a unicode encode error")		
			count += 1
			#if count >= 20:
	        	#	raw_input("\nDance the Macarena to continue")
			#	count = 0


#just showing segmentation
def show_code(aword):
	global wordmode
	if "\r" in aword:
	    aword = aword.replace("\r","\n\r")
	try:
		if wordmode:
			sys.stdout.write(' '.join(["-" , aword]))
		else:
			sys.stdout.write(''.join(["-" , aword]))
	except UnicodeEncodeError:
		    sys.stdout.write("There was an error encoding a unicode here")

def lz(s):
	global wordmode, word, code,lengthcount,runningcount
	if wordmode:
 		s = s.split(' ')
	for i in xrange(len(s)):
		nextchar = s[i]
		if wordmode:
			nextword = ' '.join([word,nextchar])
		else:
			nextword = ''.join([word,nextchar])
		if nextword in table:
			word = nextword
			table[nextword][1] += 1
		else:
			#show_code(word)
			table[nextword] = [next_code(), 1]
			word = nextchar
		#print gcount
		lengthcount += len(nextword)
		runningcount += 1
	#show_status(sys.stdout)
	
hanging = ''
maxread = 0
try:
	with codecs.open(sys.argv[1], 'r', 'utf-8') as f:
		process = hanging + f.read(4096)
		maxread += 4096
	        if wordmode:
			hanging = process.rsplit(' ', 1)[1]
		while len(process) > 0:
			try:
				lz(process)
				process = hanging + f.read(4096)
				maxread += 4096
				if wordmode:
					splits = process.rsplit(' ', 1)
					hanging = splits[1] if len(splits) > 1 else ''
				if maxread % (16777216) == 0:
					sys.stderr.write("\rRead " + `(maxread/1048576)` + " Mbytes")
					clean_dict()
			except UnicodeDecodeError:
				sys.stderr.write("Unicode decode error\n")
			except IndexError:
				sys.stderr.write("There's no hanging word or space here. That's not an error really")

	f.closed
except KeyboardInterrupt:
    clean_dict()
    sys.stderr.write("Writing " + `len(table)` + " codes . . .  ")
    with codecs.open("results.txt", 'w', 'utf-8') as fo:
 	show_status(fo)
    fo.closed
except MemoryError:
    clean_dict(2)
    sys.stderr.write("Writing " + `len(table)` + " codes . . .  ")
    with codecs.open("results.txt", 'w', 'utf-8') as fo:
 	show_status(fo)
    fo.closed
else:
    clean_dict()
    with codecs.open("results.txt", 'w', 'Big5') as fo:
 	show_status(fo)
    fo.closed




