
from math import log10, floor

def round_int(i,places):
	if i == 0:
		return 0
	isign = i/abs(i)
	i = abs(i)
	if i < 1:
		return 0
	max10exp = floor(log10(i))
	if max10exp+1 < places:
		return i
	sig10pow = 10**(max10exp-places+1)
	floated = i*1.0/sig10pow
	defloated = round(floated)*sig10pow
	return int(defloated*isign)

import sys
i = float(sys.argv[1])
print round_int(i,int(sys.argv[2]))


